import junit.framework.TestCase;

/**
 * Tests Fibonacci class.
 * @author Karol Topolski
 */
public class FibonacciTest extends TestCase {
    private static Fibonacci fib = null;
    
    public FibonacciTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        fib = new Fibonacci();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    public void testFibonacciSequence() {
        int[][] fibonacciSequence = new int[][] {
            {0, 0},
            {1, 1},
            {2, 2},
            {3, 3},
            {4, 5},
            {5, 8},
            {6, 13},
            {7, 21}
        };
        for(int i = 0; i < fibonacciSequence.length; i++)
            assertEquals(fib.next(fibonacciSequence[i][0]), fibonacciSequence[i][1]);
    }
    
    public void testNegativeNumberGuardClause() {
        try {
            fib.next(-1);
        } catch (IllegalArgumentException iae) {
            assertEquals(iae.getMessage(), "cannot calculate fibonacci series for negative number.");
        }
    }
}
