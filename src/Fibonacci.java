/**
 * Very simple Fibonacci number calculator.
 * @author Karol Topolski
 */
public class Fibonacci {
    public long next(int number) {
        if(number < 0) throw new IllegalArgumentException("cannot calculate fibonacci series for negative number.");
        if(number <= 2) return number;
        else return next(number - 1) + next(number - 2);
    }
}
