/**
 * Main class for executing fibonacci class.
 * @author Karol Topolski
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Fibonacci fib = new Fibonacci();
        for(int i = 0; i < 10; i++) {
            System.out.println("i: " + i + ", fib: " + fib.next(i));
        }
    }
}
